<?php


namespace RR\PHP\CliCommand\Command\Helper;

use RR\PHP\CliCommand\Command\AbstractCommand;
use RR\PHP\CliCommand\Console\Input\InputInterface;
use RR\PHP\CliCommand\Console\Output\OutputInterface;

/**
 * Class CommandHelper
 * @package RR\PHP\CliCommand\Command\Helper
 */
class CommandHelper
{
    public static function displayListGeneralInfo(OutputInterface $output)
    {
        $output->comment('- аргументы запуска передаются в фигурных скобках через запятую в следующем формате:');
        $output->write('  ' . '- одиночный аргумент:');
        $output->info('{arg}', true);
        $output->write('  ' . '- несколько аргументов:');
        $output->info('{arg1,arg2,arg3}', false);
        $output->write(' ИЛИ');
        $output->info('{arg1} {arg2} {arg3}');
        $output->writeln('');

        $output->comment('- параметры запуска передаются в квадратных скобках в следующем формате:');
        $output->write('  ' . '- параметр с одним значением::');
        $output->info('[name=value]', true);
        $output->write('  ' . '- параметр с несколькими значениями:');
        $output->info('[name={value1,value2,value3}]', false);
        $output->writeln('');
        $output->writeln('');
        $output->comment('- список доступных команд:', true);
    }

    /**
     * @param AbstractCommand $command
     * @param OutputInterface $output
     */
    public static function commandInfo(AbstractCommand $command, OutputInterface $output): void
    {
        $output->comment('Help ', false);
        $output->write($command->getName());
        $output->write(': ');
        $output->write($command->getDescription(), true);
        $output->writeln('');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public static function showCommandArgumentsAndOptions(InputInterface $input, OutputInterface $output): void
    {
        $output->comment('Arguments:');
        if (!$input->getArguments()) {
            $output->error('  - no arguments ');
        }

        foreach ($input->getArguments() as $argument) {
            if ($argument->getValue() === AbstractCommand::HELP_ARG) {
                continue;
            }

            $output->writeln('  - '. $argument->getValue());
        }
        $output->writeln('');
        $output->comment('Options:');
        if (!$input->getOptions()) {
            $output->error('  - no options ');
        }
        foreach ($input->getOptions() as $param) {
            $output->writeln('  - '. $param->getName());
            foreach ($param->getValues() as $value) {
                $output->writeln('    - '. $value->getValue());
            }
        }
        $output->writeln('');
    }
}
