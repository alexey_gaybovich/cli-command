<?php


namespace RR\PHP\CliCommand\Command;

use ReflectionException;
use RR\PHP\CliCommand\Console\Input\InputInterface;
use RR\PHP\CliCommand\Console\Output\OutputInterface;

abstract class AbstractCommand
{
    public const SUCCESS = 0;
    public const FAILURE = 1;
    public const HELP_ARG = 'help';

    protected static ?string $defaultName;
    private ?string $name;
    private string $description = '';


    /**
     * AbstractCommand constructor.
     * @param string|null $name
     * @throws ReflectionException
     */
    public function __construct(string $name = null)
    {
        $name = $name ?: static::getDefaultName();
        if (null !== $name || null !== $name = static::getDefaultName()) {
            $this->setName($name);
        }

        $this->configure();
    }

    protected function configure()
    {
    }

    /**
     * @throws ReflectionException
     */
    public static function getDefaultName(): ?string
    {
        $class = static::class;
        $r = new \ReflectionProperty($class, 'defaultName');
        return $class === $r->class ? static::$defaultName : null;
    }


    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return AbstractCommand
     */
    public function setName(string $name): AbstractCommand
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return AbstractCommand
     */
    public function setDescription(string $description): AbstractCommand
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    public function run(InputInterface $input, OutputInterface $output): int
    {
        $this->initialize($input, $output);
        return $this->execute($input, $output);
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function initialize(InputInterface $input, OutputInterface $output)
    {
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    abstract protected function execute(InputInterface $input, OutputInterface $output): int;
}
