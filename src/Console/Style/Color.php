<?php


namespace RR\PHP\CliCommand\Console\Style;

class Color
{
    private const COLORS = [
        'black'   => 0,
        'red'     => 1,
        'green'   => 2,
        'yellow'  => 3,
        'blue'    => 4,
        'magenta' => 5,
        'cyan'    => 6,
        'white'   => 7,
        'default' => 9,
    ];

    private ?int $textColor = 39;
    private ?int $backgroundColor = 49;

    public function __construct(string $textColor = null, string $backgroundColor = null)
    {
        if ($textColor && (null !== $textColorConsole = $this->getConsoleColor($textColor))) {
            $this->backgroundColor = $textColorConsole;
        }

        if ($backgroundColor && (null !== $bcConsole = $this->getConsoleColor($backgroundColor, true))) {
            $this->backgroundColor = $bcConsole;
        }
    }

    private function getConsoleColor(?string $color, bool $isBackground = false): ?int
    {

        $colorPrefix = $isBackground ? 4 : 3;
        if (!isset(self::COLORS[$color])) {
            return null;
        }

        return $colorPrefix . self::COLORS[$color];
    }

    public function wrapText(string $text): string
    {
        $codes = implode(';', [$this->textColor, $this->backgroundColor]);
        $open = sprintf("\033[%sm ", $codes);
        $close = "\033[0m";
        return $open . $text . $close;
    }
}
