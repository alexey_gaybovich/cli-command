<?php


namespace RR\PHP\CliCommand\Console\Style;

class ConsoleStyle implements StyleInterface
{
    const ERROR_STYLE = 'error';
    const INFO_STYLE = 'info';
    const COMMENT_STYLE = 'comment';

    private Color $color;

    public function __construct(string $textColor = null, string $backgroundColor = null)
    {
        $this->setColor(new Color($textColor, $backgroundColor));
    }


    public function getColor(): Color
    {
        return $this->color;
    }

    public function setColor(Color $color)
    {
        $this->color = $color;
    }

    public function wrapText(string $text): string
    {
        return $this->color->wrapText($text);
    }
}
