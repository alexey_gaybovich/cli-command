<?php


namespace RR\PHP\CliCommand\Console\Style;

interface StyleInterface
{
    public function getColor(): Color;

    public function setColor(Color $color);

    public function wrapText(string $text): string;
}
