<?php


namespace RR\PHP\CliCommand\Console\Input;

class ArgvInput implements InputInterface
{
    private const ARG_PATTERN = '/{(.*?)}/';
    private const OPTION_PATTERN = '/\[(.*)=(.*)]|\[(.*)]/';

    private array $rawArgs = [];

    /** @var ValueInterface[] */
    private array $arguments = [];

    /** @var OptionInterface [] */
    private array $options = [];

    /**
     * @throws InputParamException
     */
    public function __construct()
    {
        $argv = $_SERVER['argv'] ?? [];
        array_shift($argv); // исключить имя файла
        $this->rawArgs = $argv;
        $this->parseArgumentsAndParams();
    }


    /**
     * @throws InputParamException
     */
    public function parseArgumentsAndParams()
    {
        $rawArguments = $this->rawArgs;
        array_shift($rawArguments); // исключить имя команду
        foreach ($rawArguments as $rawArg) {
            $this->validate($rawArg);
            if ($this->isArgument($rawArg)) {
                $matches = $this->extractArgument($rawArg);
                $value = $matches[1] ?? $rawArg;
                $this->addArgument(new Argument($value));
            }
            if ($this->isParam($rawArg)) {
                $matches = $this->extractParam($rawArg);
                $name = $matches[1];
                $value = $matches[2];
                $this->addOption($name, $value);
            }
        }
    }

    public function extractArgument(string $rawArgument): array
    {
        preg_match(self::ARG_PATTERN, $rawArgument, $argMetches);
        return $argMetches;
    }

    public function extractParam(string $rawArgument): array
    {
        preg_match(self::OPTION_PATTERN, $rawArgument, $paramMatches);
        return $paramMatches;
    }

    public function isArgument(string $rawArgument): bool
    {
        $argMatches = $this->extractArgument($rawArgument);
        $paramMatches = $this->extractParam($rawArgument);

        if (count($argMatches) === 2 || count($paramMatches) === 0) {
            return true;
        }

        return false;
    }

    public function isParam(string $rawArgument): bool
    {
        preg_match(self::OPTION_PATTERN, $rawArgument, $optMatches);
        return count($optMatches) === 3;
    }

    /**
     * @throws InputParamException
     */
    public function validate(string $rawArgument)
    {
        $paramMatches = $this->extractParam($rawArgument);
        if (count($paramMatches) === 0) {
            return;
        }

        if (count($paramMatches) < 3 || count($paramMatches) > 3) {
            throw new InputParamException(sprintf('Неверно предан параметр (%s) команды', $rawArgument));
        }
    }

    public function getFirstArgument(): string
    {
        return current($this->rawArgs);
    }

    public function hasArgument($argument): bool
    {
        foreach ($this->getArguments() as $argumentValue) {
            if ($argument === $argumentValue->getValue()) {
                return true;
            }
        }

        return false;
    }

    public function addArgument(ValueInterface $value): InputInterface
    {
        $this->arguments[] = $value;
        return $this;
    }

    public function addOption(string $paramName, $value): InputInterface
    {
        if ($this->hasOption($paramName)) {
            $paramFromStorage = $this->getOption($paramName);
            $paramFromStorage->addValue(new Value($value));
        } else {
            $this->options[] = new Option($paramName, new Value($value));
        }
        

        return $this;
    }

    public function getOption(string $paramName): ?OptionInterface
    {
        foreach ($this->options as $key => $parsedParam) {
            if ($parsedParam->getName() === $paramName) {
                return $this->options[$key];
            }
        }

        return null;
    }

    public function hasOption(string $nameName): bool
    {
        foreach ($this->options as $parsedParam) {
            if ($parsedParam->getName() === $nameName) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return ValueInterface[]
     */
    public function getArguments(): array
    {
        return $this->arguments;
    }

    /**
     * @return OptionInterface[]
     */
    public function getOptions(): array
    {
        return $this->options;
    }
}
