<?php


namespace RR\PHP\CliCommand\Console\Input;

/**
 * Interface InputInterface
 * @package RR\PHP\CliCommand\Console\Input
 */
interface InputInterface
{
    /**
     * @return string
     */
    public function getFirstArgument(): string;

    /**
     * @param ValueInterface $value
     * @return InputInterface
     */
    public function addArgument(ValueInterface $value): InputInterface;

    /**
     * @param $argument
     * @return bool
     */
    public function hasArgument($argument): bool;

    /**
     * @param string $paramName
     * @param mixed $value
     * @return InputInterface
     */
    public function addOption(string $paramName, $value): InputInterface;

    /**
     * @param string $paramName
     * @return OptionInterface | null
     */
    public function getOption(string $paramName): ?OptionInterface;

    /**
     * @param string $nameName
     * @return bool
     */
    public function hasOption(string $nameName): bool;

    /**
     * @return ValueInterface[]
     */
    public function getArguments(): array;

    /**
     * @return OptionInterface[]
     */
    public function getOptions(): array;
}
