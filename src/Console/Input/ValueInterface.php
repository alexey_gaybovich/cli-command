<?php


namespace RR\PHP\CliCommand\Console\Input;

interface ValueInterface
{
    public function getValue();
    public function setValue($value): ValueInterface;
}
