<?php


namespace RR\PHP\CliCommand\Console\Input;

/**
 * Interface ParamInterface
 * @package RR\PHP\CliCommand\Console\Input
 */
interface OptionInterface
{

    /**
     * @param ValueInterface $value
     * @return OptionInterface
     */
    public function addValue(ValueInterface $value): OptionInterface;

    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     * @return OptionInterface
     */
    public function setName(string $name): OptionInterface;

    /**
     * @return ValueInterface[]
     */
    public function getValues(): array;
}
