<?php


namespace RR\PHP\CliCommand\Console\Input;

class Option implements OptionInterface
{
    private string $name;
    private array $values;
    
    public function __construct(string $name, ValueInterface $value)
    {
        $this->name = $name;
        $this->addValue($value);
    }


    public function addValue(ValueInterface $value): OptionInterface
    {
        $this->values[] = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Option
     */
    public function setName(string $name): Option
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ValueInterface[]
     */
    public function getValues(): array
    {
        return $this->values;
    }
}
