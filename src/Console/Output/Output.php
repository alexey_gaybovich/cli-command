<?php


namespace RR\PHP\CliCommand\Console\Output;

use Exception;
use RR\PHP\CliCommand\Console\Style\ConsoleStyle;
use RR\PHP\CliCommand\Console\Style\StyleInterface;

class Output implements OutputInterface
{
    private $stream;
    private array $styles = [];


    public function __construct()
    {
        $this->openStream();
        $this->addStyle(ConsoleStyle::INFO_STYLE, new ConsoleStyle('green'));
        $this->addStyle(ConsoleStyle::ERROR_STYLE, new ConsoleStyle('white', 'red'));
        $this->addStyle(ConsoleStyle::COMMENT_STYLE, new ConsoleStyle('yellow'));
    }


    public function openStream()
    {
        if (\STDOUT) {
            $this->stream = \STDOUT;
            return;
        }

        $this->stream = fopen('php://output', 'w');
    }

    /**
     * @return resource
     */
    public function getStream()
    {
        return $this->stream;
    }

    protected function doWrite(string $message, $newline = false)
    {
        if ($newline) {
            $message .= \PHP_EOL;
        }

        @fwrite($this->stream, $message);
        fflush($this->stream);
    }

    public function write($messages, bool $newline = false)
    {
        if (!is_iterable($messages)) {
            $messages = [$messages];
        }

        foreach ($messages as $message) {
            $this->doWrite($message, $newline);
        }
    }

    public function writeln($messages)
    {
        $this->write($messages, true);
    }

    public function addStyle(string $name, StyleInterface $style)
    {
        $this->styles[$name] = $style;
    }

    /**
     * @throws Exception
     */
    public function getStyle(string $name): ?StyleInterface
    {
        if (!isset($this->styles[strtolower($name)])) {
            throw new Exception(sprintf('Неопределенный стиль: "%s".', $name));
        }

        return $this->styles[$name];
    }

    /**
     * @throws Exception
     */
    public function info(string $text, bool $newline = true)
    {
        $text = $this->getStyle(ConsoleStyle::INFO_STYLE)->wrapText($text);
        $this->write($text, $newline);
    }

    /**
     * @throws Exception
     */
    public function comment(string $text, bool $newline = true)
    {
        $text = $this->getStyle(ConsoleStyle::COMMENT_STYLE)->wrapText($text);
        $this->write($text, $newline);
    }

    /**
     * @throws Exception
     */
    public function error(string $text, bool $newline = true)
    {
        $text = $this->getStyle(ConsoleStyle::ERROR_STYLE)->wrapText($text);
        $this->write($text, $newline);
    }
}
