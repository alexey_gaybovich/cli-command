<?php


namespace RR\PHP\CliCommand\Console\Output;

/**
 * Interface OutputInterface
 * @package RR\PHP\CliCommand\Console\Output
 */
interface OutputInterface
{

    /**
     * @param $messages
     * @param bool $newline
     */
    public function write($messages, bool $newline = false);

    /**
     * @param $messages
     */
    public function writeln($messages);

    /**
     * @param string $text
     * @param bool $newline
     */
    public function info(string $text, bool $newline = true);

    /**
     * @param string $text
     * @param bool $newline
     */
    public function comment(string $text, bool $newline = true);

    /**
     * @param string $text
     * @param bool $newline
     */
    public function error(string $text, bool $newline = true);
}
