<?php


namespace RR\PHP\CliCommand\Console;

use Exception;
use RR\PHP\CliCommand\Command\AbstractCommand;
use RR\PHP\CliCommand\Command\Helper\CommandHelper;
use RR\PHP\CliCommand\Console\Input\ArgvInput;
use RR\PHP\CliCommand\Console\Input\InputInterface;
use RR\PHP\CliCommand\Console\Output\Output;
use RR\PHP\CliCommand\Console\Output\OutputInterface;

/**
 * Class Application
 * @package RR\PHP\CliCommand\Console
 */
class Application
{
    private InputInterface $input;
    private OutputInterface $output;

    /** @var AbstractCommand[] */
    private array $commands;

    /**
     * @throws Exception
     */
    public function run(InputInterface $input = null, OutputInterface $output = null)
    {
        $this->input = $input ?? new ArgvInput();
        $this->output = $output ?? new Output();

        $isHelpCalled = $this->input->hasArgument('help');
        $commandName = $this->input->getFirstArgument();
        if (!$commandName) {
            $this->listCommands();
            return AbstractCommand::SUCCESS;
        }

        $command = $this->getCommand($commandName);

        if ($isHelpCalled) {
            CommandHelper::commandInfo($command, $this->output);
            return AbstractCommand::SUCCESS;
        }

        return $this->doRunCommand($command, $this->input, $this->output);
    }


    /**
     * Тут может находится логика обработки событий выполнения команды и перехват и обработка определенного исключения
     * @param AbstractCommand $command
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return bool
     */
    private function doRunCommand(AbstractCommand $command, InputInterface $input, OutputInterface $output): bool
    {
        return $command->run($input, $output);
    }

    /**
     * @param AbstractCommand $command
     */
    public function addCommand(AbstractCommand $command)
    {
        $this->commands[$command->getName()] = $command;
    }

    /**
     * @param string $commandName
     * @return bool
     */
    public function hasCommand(string $commandName): bool
    {
        return isset($this->commands[$commandName]);
    }

    /**
     * @throws Exception
     */
    public function getCommand($commandName): AbstractCommand
    {
        if (!$this->hasCommand($commandName)) {
            throw new Exception(sprintf('Команда (%s) не зарегистрирована в приложении', $commandName));
        }

        return $this->commands[$commandName];
    }

    private function listCommands()
    {
        CommandHelper::displayListGeneralInfo($this->output);
        foreach ($this->commands as $command) {
            $this->output->info($command->getName(), false);
            $this->output->write(' ');
            $this->output->writeln($command->getDescription());
        }
    }
}
